Welcome to App4Apps!

Note: 
Known issues with Swing GUI on different platforms. GUI was designed on macOS and appears as intended on macOS. The GUI does not display correctly on a team member's Windows machine

System requirements: 
- Must run Java 
- Have at least 2GB ram for optimal performance
- .5 GB of available storage recommended  

To get started, download the program from the repository and execute the App4Apps.jar file to start the program. Please see our user documentation for an extensive overview of our program and it's various features. 

Architectural design:
3 different types of users (all have their own .java files)
-Administrators (highest level user, can approve/deny App requests)
-Moderators (ability to remove comments)
-Customer (lowest level, can view sort and filter Apps, submit requests)

GUI.java - Most of the code for the Java swing interface is stored here, with a couple of the components having their own separate files (AppsPanel and AppsPage).

AppPage.java - Each app has it’s own page, this is where comments can be viewed added, and a more detailed description of an App can be seen. 

AppsPanel.java - The panel on the main page that displays the various apps. 

App.java - Defines what an app is. 

Driver.java - used for database testing 

main.java - contains the main method for running the program 

For more specific technical documentation, please reference the source code. You will find that each class and methods have been documented with JavaDoc for explanation of their function(s).

Feel free to reach out to the App4Apps team at anytime with questions. We are happy to help!