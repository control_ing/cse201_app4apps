
public class Moderators extends Customer {
/**
 * Constructor for Moderator
 * @param id
 * @param pw
 */
	public Moderators(String id, String pw) {
		super(id,pw); 
		
	}
	/**
	 * Moderators have the ability to delete comments 
	 * This method allows them to do so as they see fit
	 * @return
	 */
	public boolean deletePost() {
		return true;
	}
}
