import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import javax.swing.JLabel;
import javax.swing.JPanel;


/**
 * @author Bo Wang
 *
 */
@SuppressWarnings("serial")
public class AppsPanel extends JPanel {
	private ArrayList<App> apps = new ArrayList<>();

	/**
	 * Constructor to create the App panel
	 * on the main page
	 */
	
	public AppsPanel() {
		createPanel();
	}

	private void createPanel() {
		if (apps.size() != 0) {
			if (apps.size() < 5)
				this.setLayout(new GridLayout(5, 3));
			else
				this.setLayout(new GridLayout(apps.size() / 2, 3));
			for (App app : apps) {
				AppPanel appPanel = new AppPanel(app);
				this.add(appPanel);
			}
		}
	}

	public boolean addApp(App app) {
		if (apps.contains(app) || app == null)
			return false;
		apps.add(app);
		sortByName();
		return true;
	}

	public ArrayList<App> getApps() {
		return apps;
	}

	public void removeApp(App app) {
		apps.remove(app);
		sortByName();
	}

	public void removeAllApp() {
		apps.clear();
	}

	public void sortByName() {
		Collections.sort(apps, nameComparator());
		this.removeAll();
		createPanel();
	}

	public void sortByPrice() {
		Collections.sort(apps, priceComparator());
		this.removeAll();
		createPanel();
	}

	public void sortByCommons() {
		Collections.sort(apps, commonsComparator());
		this.removeAll();
		createPanel();
	}

	public void sortByPopular() {
		Collections.sort(apps, popularComparator());
		this.removeAll();
		createPanel();
	}

	Comparator<App> nameComparator() {
		return new Comparator<App>() {

			@Override
			public int compare(App o1, App o2) {
				return o1.getName().compareTo(o2.getName());
			}
		};
	}

	Comparator<App> priceComparator() {
		return new Comparator<App>() {

			@Override
			public int compare(App o1, App o2) {
				if (o1.getPrice().equals("Free") && o2.getPrice().equals("Free"))
					return 0;
				else if (o1.getPrice().equals("Free"))
					return -1;
				else if (o2.getPrice().equals("Free"))
					return 1;
				return o1.getPrice().compareTo(o2.getPrice());
			}
		};
	}

	Comparator<App> commonsComparator() {
		return new Comparator<App>() {

			@Override
			public int compare(App o1, App o2) {
				if (o1.getComments() != null && o2.getComments() != null)
					return o1.getComments().size() - o2.getComments().size();
				else if (o1.getComments() == null && o2.getComments() == null)
					return 0;
				else if (o2.getComments() == null)
					return 1;
				else
					return -1;
			}
		};
	}

	Comparator<App> popularComparator() {
		return new Comparator<App>() {

			@Override
			public int compare(App o1, App o2) {
				return o1.getFavourite() - o2.getFavourite();
			}
		};
	}

	public class AppPanel extends JPanel {
		private JLabel name;
		private JLabel description;
		private JLabel price;
		private JLabel image = new JLabel();
		private App app;
		public AppPanel(App app) {
			this.app=app;
			this.setBackground(new Color(0xFF6978));
			this.image.setIcon(app.getImage());
			ShowListener listener=new ShowListener();
			image.addMouseListener(listener);
			this.name = new JLabel(app.getName());
			name.setFont(new Font("Serif", Font.BOLD, 16));
			this.description = new JLabel(app.getCategory());
			description.setFont(new Font("Serif", Font.PLAIN, 13));
			price = new JLabel(app.getPrice());
			price.setFont(new Font("Serif", Font.BOLD, 15));
			addElements();
		}

		void addElements() {
			this.setLayout(new GridBagLayout());
			GridBagConstraints gc=new GridBagConstraints();
			
			gc.anchor=GridBagConstraints.PAGE_START;
			gc.weightx=0;
			gc.weighty=0;
			gc.gridx=0;
			gc.gridy=0;
			add(image,gc);
			
			gc.anchor=GridBagConstraints.LINE_START;
			gc.weightx=0;
			gc.weighty=0;
			gc.gridx=0;
			gc.gridy=1;
			add(name,gc);
			
			gc.gridy=2;
			add(price,gc);
			gc.gridy=3;
			add(description,gc);
		}

		private class ShowListener implements MouseListener {
			@Override
			public void mouseClicked(MouseEvent e) {
				AppPage page =new AppPage(app);
				page.setVisible(true);
			}
			@Override
			public void mousePressed(MouseEvent e) {
			}

			@Override
			public void mouseReleased(MouseEvent e) {
			}

			@Override
			public void mouseEntered(MouseEvent e) {
			}

			@Override
			public void mouseExited(MouseEvent e) {
			}
		}
	}
}
