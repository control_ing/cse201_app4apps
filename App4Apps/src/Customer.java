
public class Customer {
	private String id;
	private String pw;
	public Customer(String id, String pw) {
		this.id = id;
		this.pw = pw;
	}
	/**
	 * Add request method so that a customer can submit a request 
	 * @param appName
	 * @param platform
	 * @param version
	 * @param description
	 * @return
	 */
	
	public boolean addRequest(String appName, String platform, int version, String description) {
		// Store all these things into the database so the Admin can approve / deny 
		return false;
	}
	
	// Getters / Setters
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPw() {
		return pw;
	}
	public void setPw(String pw) {
		this.pw = pw;
	}

}
