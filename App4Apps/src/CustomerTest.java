import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Unit tests of the Customer Class
 */

public class CustomerTest {

	@Test
	public void testCustomer() {
		Customer customer=new Customer("123","password");
		assertEquals("password", customer.getPw());
		assertEquals("123", customer.getId());
	}

	@Test
	public void testAddRequest() {
		Customer customer=new Customer("123","password");
		assertFalse(customer.addRequest("", "Tester", 12, ""));
		assertTrue(customer.addRequest("Name", "IOS",1, "discription"));
	}
}
