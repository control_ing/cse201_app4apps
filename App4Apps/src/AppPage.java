
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import java.awt.Font;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JButton;

public class AppPage extends JFrame{
	private App app;
	private JTextField textField;
	/**
	 * Launch the application.
	 */
	//	public static void main(String[] args) {
	//		EventQueue.invokeLater(new Runnable() {
	//			public void run() {
	//				try {
	//					AppPage window = new AppPage(app);
	//					window.this.setVisible(true);
	//				} catch (Exception e) {
	//					e.printStackTrace();
	//				}
	//			}
	//		});
	//	}

	/**
	 * Create the application.
	 */
	public AppPage(App app) {
		this.app=app;
		initialize();
	}

	/**
	 * Initialize the contents of the this.
	 */
	private void initialize() {
		this.setBounds(200, 200, 1300, 1000);
		this.getContentPane().setLayout(null);
		this.getContentPane().setBackground(new Color(0x340068));
		JLabel lblNewLabel = new JLabel();
		lblNewLabel.setIcon(app.getImage());
		lblNewLabel.setBounds(22, 57, 199, 186);
		this.getContentPane().add(lblNewLabel);

		// labels
		
		JLabel lblName = new JLabel(app.getName());
		lblName.setForeground(Color.WHITE);
		lblName.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblName.setBounds(379, 41, 252, 48);
		this.getContentPane().add(lblName);

		JLabel lblCategory = new JLabel(app.getCategory());
		lblCategory.setForeground(Color.WHITE);
		lblCategory.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblCategory.setBounds(413, 144, 262, 48);
		this.getContentPane().add(lblCategory);

		JLabel lblDiscription = new JLabel("<html>"+app.getDescription()+"</html>");
		lblDiscription.setForeground(Color.WHITE);
		lblDiscription.setFont(new Font("Tahoma", Font.PLAIN, 25));
		lblDiscription.setBounds(258, 204, 1006, 154);
		this.getContentPane().add(lblDiscription);

		// end labels 
		
		JTextField jf = new JTextField();
		jf.setBackground(Color.WHITE);
		jf.setFont(new Font("Tahoma", Font.PLAIN, 30));
		try {
			Connection myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys", "root", "zzh951025");
			// create a statement
			Statement myStmt = myconn.createStatement();
			// execute SQL query
			ResultSet myRs = myStmt.executeQuery("SELECT Comment FROM comments where Appname = '"+app.getName()+"'");
			// process the result set
			while (myRs.next()) {
				String s = myRs.getString("Comment");
				jf.setText(s);
			}
		} catch (Exception exc) {
			exc.printStackTrace();
		}

		JScrollPane scrollPane = new JScrollPane(jf);
		scrollPane.setBounds(22, 374, 1242, 404);
		this.getContentPane().add(scrollPane);
		scrollPane.setBackground(new Color(0xFF6978));
		
		// Labels:
		
		JLabel lblPlatform = new JLabel(app.getPlatform());
		lblPlatform.setForeground(Color.WHITE);
		lblPlatform.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblPlatform.setBounds(885, 41, 240, 48);
		this.getContentPane().add(lblPlatform);

		JLabel lblAuthor = new JLabel(app.getAuthor());
		lblAuthor.setForeground(Color.WHITE);
		lblAuthor.setFont(new Font("Tahoma", Font.PLAIN, 30));
		lblAuthor.setBounds(857, 144, 407, 48);
		this.getContentPane().add(lblAuthor);

		JLabel lblName_1 = new JLabel("Name:");
		lblName_1.setForeground(Color.WHITE);
		lblName_1.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblName_1.setBounds(258, 42, 184, 48);
		getContentPane().add(lblName_1);

		JLabel categoryLbl = new JLabel("Categoryy:");
		categoryLbl.setForeground(Color.WHITE);
		categoryLbl.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		categoryLbl.setBounds(258, 145, 148, 48);
		getContentPane().add(categoryLbl);

		JLabel lblAuthor_1 = new JLabel("Author:");
		lblAuthor_1.setForeground(Color.WHITE);
		lblAuthor_1.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblAuthor_1.setBounds(697, 145, 129, 48);
		getContentPane().add(lblAuthor_1);

		JLabel lblPlatform_1 = new JLabel("Platform:");
		lblPlatform_1.setForeground(Color.WHITE);
		lblPlatform_1.setFont(new Font("Lucida Grande", Font.PLAIN, 30));
		lblPlatform_1.setBounds(697, 42, 138, 48);
		getContentPane().add(lblPlatform_1);
		
		JLabel lblNewLabel_1 = new JLabel("Comments");
		lblNewLabel_1.setForeground(Color.WHITE);
		lblNewLabel_1.setFont(new Font("Dialog", Font.PLAIN, 30));
		lblNewLabel_1.setBounds(22, 335, 157, 39);
		getContentPane().add(lblNewLabel_1);
		
<<<<<<< HEAD
		JButton btnNewButton = new JButton("Add Comments");
		btnNewButton.setBackground(Color.WHITE);
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setFont(new Font("Dialog", Font.PLAIN, 30));
		btnNewButton.setBounds(200, 338, 245, 33);
		getContentPane().add(btnNewButton);
		
		textField = new JTextField();
		textField.setBounds(506, 335, 414, 32);
		getContentPane().add(textField);
		textField.setColumns(10);
=======
		// end labels
>>>>>>> branch 'master' of https://gitlab.csi.miamioh.edu/remienjt/cse201_App4Apps.git
	}
}

