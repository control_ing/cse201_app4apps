import static org.junit.Assert.*;

import org.junit.Test;
/**
 * Tester class for the Moderator class 
 */

public class ModeratorsTest {

	@Test
	public void testModerators() {
		Moderators mo=new Moderators("id","password");
		assertEquals("id",mo.getId());
		assertEquals("password",mo.getPw());
	}

	@Test
	public void testDeletePost() {
		Moderators mo=new Moderators("id","password");
		assertFalse(mo.deletePost());
	}

}
