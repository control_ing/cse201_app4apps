import static org.junit.Assert.*;

import javax.swing.ImageIcon;

import org.junit.Test;

public class AppTest {

	@Test
	public void testApp() {
<<<<<<< HEAD
		ImageIcon i = new ImageIcon();
		App test = new App("Name","Author", "Category", "1.00",i,"Microsoft", "Description");
=======
		App test = new App("Name", "author", "Category", "1.00", new ImageIcon(),"Microsoft","Description");
>>>>>>> branch 'master' of https://gitlab.csi.miamioh.edu/remienjt/cse201_App4Apps.git
		assertFalse(test.getName().equals(""));
		assertFalse(test.getDescription().equals(""));	
	}

	@Test
	public void testAddComment() {
		App app = new App("name", "author", "category", "price", new ImageIcon(),"platform", "description");
		assertTrue(app.addComment("comment"));
	}

	@Test
	public void testRemoveComment() {
		App app0 = new App("name", "author", "category", "price", new ImageIcon(),"platform", "description");
		assertTrue(app0.removeComment("comment"));
	}

	@Test
	public void testAddFavorite() {
		App app1 = new App("name", "author", "category", "price", new ImageIcon(),"platform", "description");
		assertTrue(app1.addFavorite());
	}

	@Test 
	public void testRemoveFavorite() {
		App app2 = new App("name", "author", "category", "price", new ImageIcon(),"platform", "description");
		assertTrue(app2.removeFavorite());
	}

}
