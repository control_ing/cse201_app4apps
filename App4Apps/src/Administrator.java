import java.util.ArrayList;

public class Administrator extends Moderators {
	private ArrayList<String> requestList;
	
	/**
	 * Administrator constructor 
	 * @param id
	 * @param pw
	 */
	
	public Administrator(String id, String pw) {
		super(id,pw);
		requestList = new ArrayList<>();	
	}
	
	/**
	 * accessRequestPage()
	 * @return boolean 
	 */
	
	public boolean accessRequestPage() {
		return false;
	}
	/**
	 * Method for approving App requests
	 * @param appName
	 * @return
	 */
	
	public boolean approveRequest(String appName) {
		return false;
	}
	/**
	 * Method for denying app requests 
	 * @param appName
	 * @return
	 */
	public boolean denyRequest(String appName) {
		return false;
	}

}
