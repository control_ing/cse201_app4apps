import static org.junit.Assert.*;

import org.junit.Test;

/**Administrator Test cases 
 */

class AdministratorTest {

	@Test
	void test() {
		Administrator admin = new Administrator("testID", "testPw");
		assertFalse(admin.getId().equals(""));
		assertFalse(admin.getPw().equals(""));
	}
	@Test 
	void testAccessRequestPage() {
		Administrator admin = new Administrator("testID", "testPw");
		assertFalse(admin.getId().equals(""));
		assertFalse(admin.getPw().equals(""));
	}
	
	@Test 
	void testApproveRequest(String appName){
		Administrator admin = new Administrator("testID", "testPw");
		Customer c = new Customer("Customer","CustomerPw");
		c.addRequest("Tester", "Microsoft", 1, "Coolest App Ever!");
		assertFalse(appName.equals(""));
		assertFalse(admin.approveRequest("Tester"));
		
	}
	
	@Test 
	void denyRequest(String appName){
		Administrator admin = new Administrator("testID", "testPw");
		Customer c = new Customer("Customer","CustomerPw");
		c.addRequest("Tester", "Microsoft", 1, "Coolest App Ever!");
		assertFalse(appName.equals(""));
		assertFalse(admin.denyRequest("Tester"));
	}

}

