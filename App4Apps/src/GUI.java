import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.*;
import javax.swing.border.LineBorder;

@SuppressWarnings("serial")
/**
 * Class creates the swing GUI and the various 
 * items in it
 */

public class GUI extends JFrame {
	private JPanel panel = new filterPanel();
	private JLabel sortLabel, filters;
	private JButton signin, logout, request;
	private JComboBox<String> sort;
	private AppsPanel appPanel;
	private AppsPanel filtedApps = new AppsPanel();
	private JScrollPane scrollPane;

	/**
	 * GUI constructor 
	 */
	public GUI() {

		setTitle("App4Apps");
		setResizable(true);

		setSize(1000, 800);
		this.add(panel);
		appPanel();
		this.add(scrollPane);
		createSort();
		loginButton();
		// Pane Layout
		this.setLayout(null);
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Labels
		filters = new JLabel("Filters: ");
		filters.setForeground(new Color(0xfffcf9));
		// JButtons
		// Add to Pane
		this.add(filters);
		requestButton();
		// set position
		filters.setLocation(10, 10);
		// set size
		filters.setSize(50, 25);
		this.getContentPane().setBackground(new Color(0x340068));
	}


	public void appPanel() {
		try {
			 appPanel = new AppsPanel();
			 //get connection to database
			 Connection myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys",
			 "root", "zzh951025");
			 //create a statement
			 Statement myStmt = myconn.createStatement();
			 //execute SQL query
			 ResultSet myRs = myStmt.executeQuery("SELECT * FROM application");
			 //process the result set
			 while (myRs.next()) {
			 App app=new App(myRs.getString("Name"),myRs.getString("Author"),myRs.getString("Category"),myRs.getString("Price"),new ImageIcon(myRs.getString("Image")),myRs.getString("Platform"),myRs.getString("Description"));
		     appPanel.addApp(app);
			 }
			scrollPane = new JScrollPane(appPanel);
			scrollPane.setLocation(200, 50);
			scrollPane.setSize(750, 700);
		} catch (Exception exc) {
			exc.printStackTrace();
		}

	}
	/**
	 * login button 
	 */
	public void loginButton() {
		signin = new JButton("Sign in");
		signin.addActionListener(new signinListener());
		signin.setLocation(890, 10);
		signin.setSize(100, 30);
		signin.setBorder(new LineBorder(new Color(0xb1ede8)));
		signin.setForeground(new Color(0xfffcf9));
		logout = new JButton("log out");
		logout.addActionListener(new logoutListener());
		logout.setVisible(false);
		logout.setLocation(900, 10);
		logout.setSize(100, 30);
		logout.setBorder(new LineBorder(new Color(0xb1ede8)));
		this.add(signin);
		this.add(logout);
	}

	/**
	 * create a combo box for sort
	 */

	public void createSort() {
		sortLabel = new JLabel("Sort By:");
		sortLabel.setSize(100, 30);
		sortLabel.setLocation(200, 10);
		sortLabel.setForeground(new Color(0xfffcf9));
		sort = new JComboBox<String>();
		sort.addItem("Name");
		sort.addItem("Price");
		sort.addItem("Most popular");
		sort.addItem("Most comment");	
		sort.setSize(100, 25);
		//TODO 265
		sort.setLocation(275, 14);
		sort.addActionListener(new sortListener());
		this.add(sort);
		this.add(sortLabel);
	}

	/**
	 * create a panel for filter
	 * 
	 * @author Bo Wang
	 *
	 */
	class filterPanel extends JPanel {

		private JComboBox<String> price = new JComboBox<String>();
		private JLabel plat = new JLabel("Platform:");
		private JLabel p = new JLabel("Price:");
		private JLabel priceRange = new JLabel("Price Range:");
		private JLabel to = new JLabel("to");
		private JTextField fromPrice = new JTextField(10);
		private JTextField toPrice = new JTextField(10);
		private JComboBox<String> platForm = new JComboBox<String>();
		private JButton filter = new JButton("Filter");
		private JButton reset = new JButton("Reset");

		public filterPanel() {
			this.setLocation(10, 50);
			this.setLayout(new GridLayout(10, 1));
			this.setSize(100, 400);
			this.add(plat);
			createPlatForm();
			this.add(p);
			createPrice();
			createPriceRange();
			filterListener listener = new filterListener();
			filter.addActionListener(listener);
			filter.setBorder(new LineBorder(new Color(0xb1ede8)));
			this.add(filter);
			resetAllListener showListener = new resetAllListener();
			reset.addActionListener(showListener);
			reset.setBorder(new LineBorder(new Color(0xb1ede8)));
			this.add(reset);
		}

		public void createPlatForm() {
			this.setBackground(new Color(0xfffcf9));
			platForm.addItem("IOS");
			platForm.addItem("Android");
			platForm.addItem("Windows");
			platForm.addItem("Mac");
			this.add(platForm);
		}

		public void createPrice() {
			price.addItem("Free");
			price.addItem("Paid");
			rangeShowListener show = new rangeShowListener();
			price.addActionListener(show);
			this.add(price);
		}

		public void createPriceRange() {
			priceRange.setVisible(false);
			fromPrice.setVisible(false);
			to.setVisible(false);
			toPrice.setVisible(false);
			this.add(priceRange);
			fromPrice.setSize(10, 30);
			this.add(fromPrice);
			this.add(to);
			this.add(toPrice);
		}

		/** 
		 * Reset button 
		 */
		private class resetAllListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				filtedApps.removeAllApp();
				scrollPane.setViewportView(appPanel);
			}

		}

		private class rangeShowListener implements ActionListener {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (price.getSelectedItem().equals("Paid")) {
					priceRange.setVisible(true);
					fromPrice.setVisible(true);
					to.setVisible(true);
					toPrice.setVisible(true);
				} else {
					priceRange.setVisible(false);
					fromPrice.setVisible(false);
					to.setVisible(false);
					toPrice.setVisible(false);
				}

			}

		}

		private class filterListener implements ActionListener {
			@Override
			public void actionPerformed(ActionEvent e) {
				filtedApps.removeAllApp();
				for (App app : appPanel.getApps()) {
					if (price.getSelectedItem().equals("Free")) {
						if (app.getPrice().equals("Free"))
							filtedApps.addApp(app);
					} else if (!app.getPrice().equals("Free")) {
						if (!fromPrice.getText().equals("") && !toPrice.getText().equals("")) {
							// System.out.println(fromPrice.getText()+"\n"+toPrice.getText());
							double begin = Double.parseDouble(fromPrice.getText());
							double end = Double.parseDouble(toPrice.getText());
							Double price = Double.parseDouble(app.getPrice().substring(1));
							if (price < end && price > begin)
								filtedApps.addApp(app);
						}

						else if (!fromPrice.getText().equals("")) {
							double begin = Double.parseDouble(fromPrice.getText());
							Double price = Double.parseDouble(app.getPrice().substring(1));
							if (price >= begin)
								filtedApps.addApp(app);
						}

						else if (!toPrice.getText().equals("")) {
							double end = Double.parseDouble(fromPrice.getText());
							Double price = Double.parseDouble(app.getPrice().substring(1));
							if (price <= end)
								filtedApps.addApp(app);
						} else
							filtedApps.addApp(app);
					}
				}
				String plateform = (String) platForm.getSelectedItem();
				int i = 0;
				while (i < filtedApps.getApps().size()) {
					if (!filtedApps.getApps().get(i).getPlatform().equals(plateform)) {
						filtedApps.removeApp(filtedApps.getApps().get(i));
					} else {
						i++;
					}
				}
				// for(int c=0;c<temp.getApps().size();c++){
				// System.out.println(temp.getApps().get(c).getName());
				// }
				scrollPane.setViewportView(filtedApps);
			}
		}
	}

	private class sortListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (filtedApps.getApps().size() == 0) {
				String current = (String) sort.getSelectedItem();
				if (current.equals("Name")) {
					appPanel.sortByName();
					scrollPane.setViewportView(appPanel);
				} else if (current.equals("Price")) {
					appPanel.sortByPrice();
					scrollPane.setViewportView(appPanel);
				} else if (current.equals("Most Popular")) {
					appPanel.sortByPopular();
					scrollPane.setViewportView(appPanel);
				} else {
					appPanel.sortByCommons();
					scrollPane.setViewportView(appPanel);
				}
			} else {
				String current = (String) sort.getSelectedItem();
				if (current.equals("Name")) {
					filtedApps.sortByName();
				} else if (current.equals("Price")) {
					filtedApps.sortByPrice();
				} else if (current.equals("Most Popular")) {
					filtedApps.sortByPopular();
				} else {
					filtedApps.sortByCommons();
				}
			}
		}

	}

	private class signinListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			JFrame logins = new JFrame();
			String username = JOptionPane.showInputDialog(logins, "Please enter your username");
			String password = JOptionPane.showInputDialog(logins, "Please enter your password");
			boolean flag = false;
			try {
				// get connection to database
				Connection myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys", "root", "zzh951025");
				// create a statement
				Statement myStmt = myconn.createStatement();
				// execute SQL query
				ResultSet myRs = myStmt.executeQuery("SELECT * FROM login WHERE username = '" + username + "'");
				// process the result set
				while (myRs.next()) {
					if ((myRs.getString("username")).equals(username) && (myRs.getString("password").equals(password)))
						flag = true;
				}
				if (flag) {
					signin.setVisible(false);
					logout.setVisible(true);
					request.setVisible(true);
				} else {
					JOptionPane.showMessageDialog(logins, "Incorrect username or password !");
				}

			} catch (Exception exc) {
				exc.printStackTrace();
			}
		}
	}

	private class logoutListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			JFrame logins = new JFrame();
			signin.setVisible(true);
			logout.setVisible(false);
			request.setVisible(false);
			JOptionPane.showMessageDialog(logins, "You have already logged out");
		}

	}

	public void requestButton() {
		request = new JButton("Request");
		request.addActionListener(new requestListener());
		request.setLocation(700, 10);
		request.setSize(100, 30);
		request.setVisible(false);
		this.add(request);
	}

	private class requestListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			RequestPage request = new RequestPage();
			request.setVisible(true);
		}
	}

	class RequestPage extends JFrame {
		private requestPane rp = new requestPane();

		public RequestPage() {
			this.setTitle("Request Page");
			this.setSize(1000, 800);
			this.add(rp);
			setVisible(true);
		}

		class requestPane extends JPanel {
			private JTextField nam = new JTextField(10);
			private JTextField pric = new JTextField(10);
			private JTextField categor = new JTextField(10);
			private JTextField imag = new JTextField(10);
			private JTextField autho = new JTextField(10);
			private JTextField platfor = new JTextField(10);
			private JTextField descriptio = new JTextField(20);
			private JLabel name = new JLabel("Name: ");
			private JLabel price = new JLabel("Price: ");
			private JLabel category = new JLabel("Catefory: ");
			private JLabel image = new JLabel("Image: ");
			private JLabel auth = new JLabel("Author: ");
			private JLabel plat = new JLabel("Platform: ");
			private JLabel descri = new JLabel("Description: ");
			private JButton submit = new JButton("Submit");


			public requestPane(){
				this.setLocation(10,50);
				this.setLayout(new GridLayout(10,1));

				this.setSize(500, 800);
				createRequest();
				submit.addActionListener(new submitListener());
				this.add(submit);
			}


			private class submitListener implements ActionListener{

				public void actionPerformed(ActionEvent e) {
					try {
						String na = nam.getText();
						String pr = pric.getText();
						String ca = categor.getText();
						String im = imag.getText();
						String author = autho.getText();
						String platform = platfor.getText();
						String description = descriptio.getText();
						//get connection to database
						Connection myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys", "root", "zzh951025");
						//create a statement
						Statement myStmt = myconn.createStatement();
						//execute SQL query 
						String query = "insert into sys.application (Name,Author,Price,Category,Image,Platform,Description)" + " values (?,?,?,?,?,?,?)";

						PreparedStatement preparedStmt = myconn.prepareStatement(query);
						preparedStmt.setString (1, na);
						preparedStmt.setString (2, author);
						preparedStmt.setString (3, pr);
						preparedStmt.setString (4, ca);
						preparedStmt.setString (5, im);
						preparedStmt.setString (6, platform);
						preparedStmt.setString (7, description);
						preparedStmt.execute();
						myconn.close();
						appPanel();
						rp.setVisible(false);
					}
					catch (Exception exc) {
						exc.printStackTrace();
					}
				}
			}


			public void createRequest() {
				this.add(name);
				this.add(nam);
				this.add(price);
				this.add(pric);
				this.add(category);
				this.add(categor);
				this.add(image);
				this.add(imag);
				this.add(auth);
				this.add(autho);
				this.add(plat);
				this.add(platfor);
				this.add(descri);
				this.add(descriptio);
			}
		}
	}

	/**
	 * Borders for various element in the swing GUI window
	 */
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(new Color(0xb1ede8));
		// Left side bar 
		g.drawRoundRect(3,25,115,455,30,30);
		// sort by 
		g.drawRoundRect(190, 30, 190, 35, 30, 30);
	}
}