-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: sys
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `application`
--

DROP TABLE IF EXISTS `application`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `application` (
  `Name` varchar(45) NOT NULL,
  `Price` varchar(45) NOT NULL,
  `Category` varchar(100) NOT NULL,
  `Image` varchar(1000) NOT NULL,
  `Author` varchar(45) NOT NULL,
  `Platform` varchar(45) NOT NULL,
  `Description` varchar(1000) NOT NULL,
  PRIMARY KEY (`Name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `application`
--

LOCK TABLES `application` WRITE;
/*!40000 ALTER TABLE `application` DISABLE KEYS */;
INSERT INTO `application` VALUES ('1','1','1','src\\\\test.jpg','1','1','1'),('Adobe Photoshop','$200','Design','src\\photoshop.png','Adobe System','Windows','Adobe Photoshop is a raster graphics editor developed and published by Adobe Systems for macOS and Windows.'),('Angry Bird','$20','Game','src\\angrybirds.png','Rovio Entertainment','Windows','Angry Birds is a casual puzzle video game developed by Rovio Entertainment. Inspired primarily by a sketch of stylized wingless birds, the game was first released for iOS and Maemo devices.'),('Eclipse','Free','Developer','src\\eclipse.png','Eclipse Foundation','Windows','Eclipse is an integrated development environment (IDE) used in computer programming, and is the most widely used Java IDE.'),('Facebook','Free','Social Networking','src\\facebook.png','Mark Zuckerberg','Windows','Facebook is an online social media and social networking service.'),('GarageBand','Free','Music','src\\garageband.png','Apple Inc.','IOS','GarageBand is a line of digital audio workstations for macOS and iOS that allows users to create music or podcasts. '),('Instagram','Free','Social Networking','src\\instagram.png','Facebook Inc.','Windows','Instagram is a mobile, desktop, and Internet-based photo-sharing application and service that allows users to share pictures and videos either publicly, or privately to pre-approved followers.'),('Snap Chat','Free','Social Networking','src\\snapchat.png','Snap Inc.','Windows','Snapchat is an image messaging and multimedia mobile application created and developed by Snap Inc., originally Snapchat Inc.'),('Steam','Free','Game','src\\steam.png','Steam Inc.','Windows','Steam is a digital distribution platform developed by Valve Corporation, which offers digital rights management (DRM), multiplayer gaming, video streaming and social networking services.'),('Twitter','Free','Social Networking','src\\twitter.png','Jack Dorsey','Windows','Twitter is an online news and social networking service where users post and interact with messages, called \"tweets.\" '),('VLC','Free','Video','src\\vlc.png','VideoLAN','Windows','VLC media player (commonly known as VLC) is a free and open-source, portable and cross-platform media player and streaming media server developed by the VideoLAN project.');
/*!40000 ALTER TABLE `application` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `Comment` varchar(200) NOT NULL,
  `Appname` varchar(45) NOT NULL,
  PRIMARY KEY (`Comment`,`Appname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES ('Excellent app','Facebook'),('good app','Angrybirds'),('Incredible app','Angry Bird'),('Very good app','Eclipse');
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `login`
--

DROP TABLE IF EXISTS `login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `login` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  PRIMARY KEY (`username`,`password`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `login`
--

LOCK TABLES `login` WRITE;
/*!40000 ALTER TABLE `login` DISABLE KEYS */;
INSERT INTO `login` VALUES ('alex','12345'),('bo','12345'),('carter','12345'),('cliff','12345'),('jerry','12345');
/*!40000 ALTER TABLE `login` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sys_config`
--

DROP TABLE IF EXISTS `sys_config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sys_config` (
  `variable` varchar(128) NOT NULL,
  `value` varchar(128) DEFAULT NULL,
  `set_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `set_by` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`variable`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sys_config`
--

LOCK TABLES `sys_config` WRITE;
/*!40000 ALTER TABLE `sys_config` DISABLE KEYS */;
INSERT INTO `sys_config` VALUES ('diagnostics.allow_i_s_tables','OFF','2017-11-08 01:05:10',NULL),('diagnostics.include_raw','OFF','2017-11-08 01:05:10',NULL),('ps_thread_trx_info.max_length','65535','2017-11-08 01:05:10',NULL),('statement_performance_analyzer.limit','100','2017-11-08 01:05:10',NULL),('statement_performance_analyzer.view',NULL,'2017-11-08 01:05:10',NULL),('statement_truncate_len','64','2017-11-08 01:05:10',NULL);
/*!40000 ALTER TABLE `sys_config` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-12-08 18:18:00
