

import java.sql.*;
public class Driver {
/**
 * Driver file to test connection with the database 	
 * Not a component of the App4Apps program. 
 */
	public static void main(String[] args) {
		
		try {
			//get connection to database
			Connection myconn = DriverManager.getConnection("jdbc:mysql://localhost:3306/sys", "root", "zzh951025");
			//create a statement
			Statement myStmt = myconn.createStatement();
			//execute SQL query 
			ResultSet myRs = myStmt.executeQuery("SELECT * FROM application ");
			//process the result set
			while (myRs.next()) {
				System.out.println(myRs.getString("Nam")+ ", " + myRs.getString("Price")+", " + myRs.getString("Description")+myRs.getString("Image"));
			}
			System.out.println(myRs.first());
			
		}
		catch (Exception exc) {
			exc.printStackTrace();
		}
	}
}
