import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;

public class App {
	private int favorite;
	private String platform;
	private String name;
	private String description;
	private String category;
	private String price; 
	private ImageIcon image;
	private String author;
	private ArrayList<String> comments;
	/**
	 * create a app with specific name description and price
	 * @param name the name of the app
	 * @param description simple description of the app
	 * @param price the price of the app 
	 */

	public App(String name, String author, String category, String price, ImageIcon imageIcon,String platform,String description) {
		this.author=author;
		this.name=name;
		this.category=category;
		this.price=price;
		this.platform=platform;
		this.description=description;
		Image image = imageIcon.getImage();  
		Image newimg = image.getScaledInstance(130, 130,  java.awt.Image.SCALE_SMOOTH); // scale it the smooth way  
		imageIcon = new ImageIcon(newimg); 
		this.image=imageIcon;
	}
	/**
	 * add a comment to the app
	 * @param The comment to be added 
	 */
	public boolean addComment(String comment){
		comments.add(comment);
		return false;
	}
	/**
	 * method to remove a specified comment 
	 * @param comment to be removed 
	 */
	public boolean removeComment(String comment){
		return false;
	}
	/**
	 * increment the favorite count 
	 */
	public boolean addFavorite(){
		return false;
	}
	/**
	 * decrement the favorite count
	 * @return 
	 */
	public boolean removeFavorite() {
		return false;
	}
	// Getters 
	
	public String getName() {
		return name;
	}
	public String getCategory() {
		return category;
	}
	public String getPrice() {
		return price;
	}
	public ImageIcon getImage() {
		return image;
	}
	public ArrayList<String> getComments() {
		return comments;
	}
	public int getFavourite() {
		return favorite;
	}
	public String getPlatform() {
		return platform;
	}
	public String getDescription() {
		return description;
	}
	public String getAuthor() {
		return author;
	}
}
